# MONITORING DOCKER CONTAINER METRICS USING CADVISOR

The following documentation is purely reproduced.
[Monitoring Docker container metrics using cAdvisor | Prometheus](https://prometheus.io/docs/guides/cadvisor/)

# Using

1. Execute `docker-compose up -d` in a terminal.
2. Visit http://localhost:8080 in your browser
